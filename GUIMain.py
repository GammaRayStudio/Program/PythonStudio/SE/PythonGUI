import wx
import wxGUI


class GUIFrame(wxGUI.MyFrame1):
    def __init__(self, parent):
        wxGUI.MyFrame1.__init__(self, parent)

    def OnBtnClick(self, event):
        msg = self.m_textCtrl1.GetValue()
        self.m_staticText1.SetLabel(msg)


if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    app = wx.App()
    frm = GUIFrame(None)
    frm.Show()
    app.MainLoop()
