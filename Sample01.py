
#!/usr/bin/env python
"""
Hello World, but with more meat.
"""

import wx


class HelloFrame(wx.Frame):
    """
    A Frame that says Hello World
    """

    def __init__(self, *args, **kw):
        # ensure the parent's __init__ is called
        super(HelloFrame, self).__init__(*args, **kw)

        # create a panel in the frame
        pnl = wx.Panel(self)
        text = wx.StaticText(pnl, -1, "Sample 01")
        t1 = wx.TextCtrl(pnl, -1, "")

        self.tx = text
        self.tc1 = t1

        b = wx.Button(pnl, 10, "Button")
        # and create a sizer to manage the layout of child widgets
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(text, wx.SizerFlags().Border(wx.TOP | wx.LEFT, 25))
        sizer.Add(t1, wx.SizerFlags().Border(wx.TOP | wx.LEFT, 25))
        sizer.Add(b, wx.SizerFlags().Border(wx.TOP | wx.LEFT, 25))
        pnl.SetSizer(sizer)

        self.Bind(wx.EVT_BUTTON, self.OnClick, b)

    def OnClick(self, event):
        msg = self.tc1.GetValue()
        self.tx.SetLabel(msg)


if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    app = wx.App()
    frm = HelloFrame(None, title='Sample 01')
    frm.Show()
    app.MainLoop()
